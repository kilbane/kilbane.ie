---
title: "Calculator"
date: 2023-07-16T14:10:28+01:00
draft: false
code: "https://gitlab.com/kilbane/calculator"
---
I made a simple calculator app to teach myself HTML, CSS and JavaScript. You
can open it in a browser and do basic arithmetic operations. [Try it
yourself!](https://kilbane.gitlab.io/calculator)
