---
title: "Up"
date: 2024-05-06T17:29:03+01:00
draft: false
code: "https://gitlab.com/kilbane/up"
---
A script for interacting with GNU pass through dmenu.

## Dependencies
- pass
- dmenu
- xclip
- find
- sed
- echo
- st
- openssh (optional, required for syncing)

## Setup
1. Create a link from `up.sh` to somewhere on your path called `up`.
2. Create a link from `pass_ssh.sh` to somewhere on your path called `pass_ssh`.
3. If you have syncing set up, store the passphrase for your ssh key in `~/.password-store/ssh.gpg`.

## Usage
### Copying a password
1. Run `up`.
2. Select the service from the menu.
3. Select "Copy".
4. up will look for a username on line 2 of the credential file for the
   selected service, and copy it into your clipboard if it exists.
5. Once you paste the username, up will look for a password on line 1 of the
   credential file and copy it into your clipboard.

### Editing a password
1. Run `up`.
2. Select the service from the menu.
3. Select "Edit".
4. Edit your password in the text editor.

### Generating a password
1. Run `up`.
2. Type in the name of the service you want to generate a password for.
3. If the password exists, select "Generate", otherwise go to the next step.
3. Select "Yes" from the menu.
4. Your new password will be generated.
