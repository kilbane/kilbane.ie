---
title: "Scrabble Anagram Solver"
date: 2023-07-16T14:10:46+01:00
draft: false
code: "https://gitlab.com/kilbane/scrabble-anagram-solver"
---
This is a C program that I wrote to determine every word that can be made from a set of Scrabble tiles. It even incorporates blank tiles! 

Cheating is bad, so **DO NOT** use this in a real game of Scrabble without consulting your opponent!

## To build:
```
make
```

## To run:
```
./anagram <letters>
```
Here, `<letters>` is the combnation of letters that you have at your disposal.
Blank tiles are represented by `?`.
Run this command to get a list of all possible words you can make with your letters and the score for each word.
